#Enter the values of k and m
#----------------------------------------------------------------------------------------------------------------
k = input("Enter the value of k: ")
m = input("Enter the value of m: ")
#----------------------------------------------------------------------------------------------------------------
#Compute for Multiplication Method 
A = random.random()  # random between [0,1)
number=int(k)*A
number_dec = str(number-int(number))[1:]
print('--------------Multiplication Method---------------------------')
print('The value h(k) for ', m, ' and ', k, ' is ', math.ceil(int(m)*float(number_dec)), '.', sep='')